package com.space.control.command;

import com.space.control.ControlSession;
import com.space.control.Display;
import com.space.grid.terrain.GridTerrain;
import com.space.grid.terrain.GridTerrainPosition;
import com.space.rover.Rover;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.Mockito.mock;

class GridConstructCommandTest {

    private GridConstructCommand command;
    private ControlSession session;

    @BeforeEach
    void setUp() {
        command = new GridConstructCommand();
        session = new ControlSession();
        command.setSession(session);
        command.setDisplay(mock(Display.class));
    }

    @Test
    void pattern_valid1() {
        assertThat(command.getPattern().matcher("5 5").matches()).isTrue();
    }

    @Test
    void pattern_valid2() {
        assertThat(command.getPattern().matcher("25  533").matches()).isTrue();
    }

    @Test
    void pattern_invalid1() {
        assertThat(command.getPattern().matcher("25.3 533").matches()).isFalse();
    }

    @Test
    void pattern_invalid2() {
        assertThat(command.getPattern().matcher("25.3 D").matches()).isFalse();
    }

    @Test
    void pattern_invalid3() {
        assertThat(command.getPattern().matcher("").matches()).isFalse();
    }

    @Test
    void initializeSession() {
        command.execute("5 5");
        assertThat(session.getTerrain() instanceof GridTerrain).isTrue();
        GridTerrain terrain = (GridTerrain) session.getTerrain();
        assertThat(terrain.withinBounds(new GridTerrainPosition(6, 6))).isFalse();
    }

    @Test
    void reInitializeSession() {
        command.execute("5 5");
        assertThat(session.getTerrain() instanceof GridTerrain).isTrue();
        session.addRover(mock(Rover.class));

        command.execute("6 6");
        GridTerrain terrain = (GridTerrain) session.getTerrain();
        assertThat(terrain.withinBounds(new GridTerrainPosition(6, 6))).isTrue();
        assertThat(session.getRover()).isNull();
    }
}