package com.space.control.command;

import com.google.common.collect.ImmutableList;
import com.google.common.truth.Truth;
import com.space.control.ControlSession;
import com.space.control.Display;
import com.space.grid.rover.*;
import com.space.grid.terrain.GridHeading;
import com.space.grid.terrain.GridTerrainPosition;
import com.space.rover.Rover;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatcher;
import org.mockito.Mockito;

import java.util.List;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.mock;

class NavigateGridRoverCommandTest {

    private NavigateGridRoverCommand command;
    private ControlSession session;
    private Display display;

    @BeforeEach
    void setUp() {
        command = new NavigateGridRoverCommand();
        display = mock(Display.class);
        command.setDisplay(display);
        session = new ControlSession();
        command.setSession(session);
    }

    @Test
    void pattern_valid1() {
        assertThat(command.getPattern().matcher("LLRRMMLMMRL").matches()).isTrue();
    }

    @Test
    void pattern_valid2() {
        assertThat(command.getPattern().matcher("LlRrMmMRlrrll").matches()).isTrue();
    }

    @Test
    void pattern_invalid1() {
        assertThat(command.getPattern().matcher("LlR rMmMR lrrll").matches()).isFalse();
    }

    @Test
    void navigateWithoutTerrain() {
        command.execute("LM");
        verify(display).showMessage(anyString());
    }

    @Test
    void navigateNonGridRover() {
        Rover rover = mock(Rover.class);
        session.addRover(rover);
        command.execute("LM");
        verify(rover, never()).perform(anyList());
    }

    @Test
    void navigateGridRover() {
        GridRover rover = mock(GridRover.class);
        GridTerrainPosition position = new GridTerrainPosition();
        when(rover.getCurrentPosition()).thenReturn(position);
        when(rover.getHeading()).thenReturn(GridHeading.NORTH);
        session.addRover(rover);
        command.execute("LMR");

        verify(rover).perform(argThat((ArgumentMatcher<List<GridRoverCommand>>)
                argument -> argument.size() == 3 &&
                        argument.get(0) instanceof GridRoverTurnLeft &&
                        argument.get(1) instanceof GridRoverMove &&
                        argument.get(2) instanceof GridRoverTurnRight
        ));
    }
}