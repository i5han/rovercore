package com.space.control.command;

import com.google.common.collect.ImmutableList;
import com.space.control.ControlSession;
import com.space.control.Display;
import com.space.grid.rover.GridRoverFactoryImpl;
import com.space.grid.rover.GridRoverImpl;
import com.space.grid.terrain.GridHeading;
import com.space.grid.terrain.GridTerrain;
import com.space.grid.terrain.GridTerrainPosition;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.Mockito.mock;

class AddGridRoverCommandTest {

    private AddGridRoverCommand command;
    private ControlSession session;

    @BeforeEach
    void setUp() {
        command = new AddGridRoverCommand();
        session = new ControlSession();
        command.setSession(session);
        command.setDisplay(mock(Display.class));
        command.setRoverFactory(new GridRoverFactoryImpl());
    }

    @Test
    void pattern_valid1() {
        ImmutableList<String> headings = ImmutableList.of("N", "E", "W", "S");
        for (String heading : headings) {
            assertThat(command.getPattern().matcher("1 1 " + heading).matches()).isTrue();
            assertThat(command.getPattern().matcher("1 1 " + heading.toLowerCase()).matches()).isTrue();
        }
    }

    @Test
    void pattern_valid2() {
        assertThat(command.getPattern().matcher("1233 12 s").matches()).isTrue();
    }

    @Test
    void pattern_valid3() {
        assertThat(command.getPattern().matcher("1233   12   E").matches()).isTrue();
    }

    @Test
    void pattern_invalid1() {
        assertThat(command.getPattern().matcher("1.1 2 S").matches()).isFalse();
    }

    @Test
    void pattern_invalid2() {
        assertThat(command.getPattern().matcher("1 S").matches()).isFalse();
    }

    @Test
    void pattern_invalid3() {
        assertThat(command.getPattern().matcher("1 3 F").matches()).isFalse();
    }

    @Test
    void addRoverWithoutTerrain() {
        command.execute("1 1 N");
        assertThat(session.getRover()).isNull();
    }

    @Test
    void addRoverWithTerrain() {
        session.initialize(new GridTerrain(5, 5));
        command.execute("1 2 N");
        assertThat(session.getRover() instanceof GridRoverImpl).isTrue();

        GridRoverImpl rover = (GridRoverImpl) session.getRover();
        GridTerrainPosition position = rover.getCurrentPosition();
        assertThat(position.getX()).isEqualTo(1);
        assertThat(position.getY()).isEqualTo(2);
        assertThat(rover.getHeading()).isEqualTo(GridHeading.NORTH);
    }
}