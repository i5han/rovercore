package com.space.control.command;

import com.google.common.collect.ImmutableSet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.regex.Pattern;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class CommandFactoryTest {

    private CommandFactory factory;

    private Command mockCommand;
    private InvalidCommand invalidCommand;

    @BeforeEach
    void setUp() {
        factory = new CommandFactory();

        mockCommand = mock(Command.class);
        when(mockCommand.getPattern()).thenReturn(Pattern.compile("\\d+\\s\\d"));

        factory.setCommands(ImmutableSet.of(mockCommand));
        invalidCommand = new InvalidCommand();
        factory.setInvalidCommand(invalidCommand);
    }

    @Test
    void validCommand() {
        Command command = factory.getCommand("12 2");
        assertThat(command).isEqualTo(mockCommand);
    }

    @Test
    void invalidCommand() {
        Command command = factory.getCommand("lslsls");
        assertThat(command).isEqualTo(invalidCommand);
    }
}