package com.space.grid.terrain;

import org.junit.jupiter.api.Test;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

class GridTerrainTest {

    @Test
    void constructionWithNegativeX() {
        assertThrows(
                IllegalArgumentException.class,
                () -> new GridTerrain(-1, 0)
        );
    }

    @Test
    void constructionWithNegativeY() {
        assertThrows(
                IllegalArgumentException.class,
                () -> new GridTerrain(0, -2)
        );
    }

    @Test
    void withinBounds_within() {
        GridTerrain terrain = new GridTerrain(5, 10);
        assertThat(terrain.withinBounds(new GridTerrainPosition(2, 8))).isTrue();
    }

    @Test
    void withinBounds_onBorder() {
        GridTerrain terrain = new GridTerrain(5, 10);
        assertThat(terrain.withinBounds(new GridTerrainPosition(5, 10))).isTrue();
    }

    @Test
    void withinBounds_outOfXRange() {
        GridTerrain terrain = new GridTerrain(5, 10);
        assertThat(terrain.withinBounds(new GridTerrainPosition(8, 5))).isFalse();
    }

    @Test
    void withinBounds_outOfYRange() {
        GridTerrain terrain = new GridTerrain(5, 10);
        assertThat(terrain.withinBounds(new GridTerrainPosition(2, 16))).isFalse();
    }

    @Test
    void withinBounds_outOfXRangeNegative() {
        GridTerrain terrain = new GridTerrain(5, 10);
        assertThat(terrain.withinBounds(new GridTerrainPosition(-2, 5))).isFalse();
    }

    @Test
    void withinBounds_outOfYRangeNegative() {
        GridTerrain terrain = new GridTerrain(5, 10);
        assertThat(terrain.withinBounds(new GridTerrainPosition(6, -5))).isFalse();
    }

    @Test
    void blockPosition() {
        GridTerrain terrain = new GridTerrain(5, 10);
        terrain.blockPosition(new GridTerrainPosition(2, 3));
        assertThat(terrain.isBlocked(new GridTerrainPosition(2, 3))).isTrue();
    }

    @Test
    void blockPosition_outOfRange() {
        GridTerrain terrain = new GridTerrain(5, 10);
        assertThrows(
                IllegalArgumentException.class,
                () -> terrain.blockPosition(new GridTerrainPosition(6, 8))
        );
    }

    @Test
    void unblockPosition() {
        GridTerrain terrain = new GridTerrain(5, 10);
        terrain.blockPosition(new GridTerrainPosition(2, 3));
        terrain.unblockPosition(new GridTerrainPosition(2, 3));
        assertThat(terrain.isBlocked(new GridTerrainPosition(2, 3))).isFalse();
    }

    @Test
    void unblockPosition_outOfRange() {
        GridTerrain terrain = new GridTerrain(5, 10);
        assertThrows(
                IllegalArgumentException.class,
                () -> terrain.unblockPosition(new GridTerrainPosition(6, 8))
        );
    }

    @Test
    void isBlocked_notBlocked() {
        GridTerrain terrain = new GridTerrain(5, 10);
        assertThat(terrain.isBlocked(new GridTerrainPosition(2, 3))).isFalse();
    }

    @Test
    void isBlocked_notBlocked_xAxis() {
        GridTerrain terrain = new GridTerrain(5, 10);
        assertThat(terrain.isBlocked(new GridTerrainPosition(5, 3))).isFalse();
    }

    @Test
    void isBlocked_notBlocked_yAxis() {
        GridTerrain terrain = new GridTerrain(5, 10);
        assertThat(terrain.isBlocked(new GridTerrainPosition(3, 10))).isFalse();
    }

    @Test
    void isBlocked_outOfRange() {
        GridTerrain terrain = new GridTerrain(5, 10);
        assertThrows(
                IllegalArgumentException.class,
                () -> terrain.isBlocked(new GridTerrainPosition(6, 8))
        );
    }

    @Test
    void isPositionValid() {
        GridTerrain terrain = new GridTerrain(5, 10);
        assertThat(terrain.isPositionValid(new GridTerrainPosition(3, 6))).isTrue();
    }

    @Test
    void isPositionValid_outOfRange() {
        GridTerrain terrain = new GridTerrain(5, 10);
        assertThat(terrain.isPositionValid(new GridTerrainPosition(6, 6))).isFalse();
    }

    @Test
    void isPositionValid_blocked() {
        GridTerrain terrain = new GridTerrain(5, 10);
        terrain.blockPosition(new GridTerrainPosition(3, 3));
        assertThat(terrain.isPositionValid(new GridTerrainPosition(3, 3))).isFalse();
    }
}