package com.space.grid.terrain;


import org.junit.jupiter.api.Test;

import static com.google.common.truth.Truth.assertThat;

class GridTerrainPositionTest {

    @Test
    void getX() {
        GridTerrainPosition position = new GridTerrainPosition();
        assertThat(position.getX()).isEqualTo(0);
    }

    @Test
    void getY() {
        GridTerrainPosition position = new GridTerrainPosition(5, 63);
        assertThat(position.getY()).isEqualTo(63);
    }

    @Test
    void equals_sameInstance() {
        GridTerrainPosition position = new GridTerrainPosition(5, 63);
        assertThat(position.equals(position)).isTrue();
    }

    @Test
    void equals_null() {
        GridTerrainPosition position = new GridTerrainPosition(5, 63);
        assertThat(position.equals(null)).isFalse();
    }

    @Test
    void equals_differentType() {
        GridTerrainPosition position = new GridTerrainPosition(5, 63);
        assertThat(position.equals("test")).isFalse();
    }

    @Test
    void equals_differentEqualInstance() {
        GridTerrainPosition position1 = new GridTerrainPosition(5, 63);
        GridTerrainPosition position2 = new GridTerrainPosition(5, 63);
        assertThat(position1.equals(position2)).isTrue();
    }

    @Test
    void equals_differentUnequalYInstance() {
        GridTerrainPosition position1 = new GridTerrainPosition(5, 63);
        GridTerrainPosition position2 = new GridTerrainPosition(5, 65);
        assertThat(position1.equals(position2)).isFalse();
    }

    @Test
    void equals_differentUnequalXInstance() {
        GridTerrainPosition position1 = new GridTerrainPosition(5, 63);
        GridTerrainPosition position2 = new GridTerrainPosition(6, 63);
        assertThat(position1.equals(position2)).isFalse();
    }

    @Test
    void equals_differentUnequalXYInstance() {
        GridTerrainPosition position1 = new GridTerrainPosition(5, 63);
        GridTerrainPosition position2 = new GridTerrainPosition(3, 3);
        assertThat(position1.equals(position2)).isFalse();
    }
}