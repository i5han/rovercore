package com.space.grid.rover;

import com.space.grid.terrain.GridHeading;
import com.space.grid.terrain.GridTerrain;
import com.space.grid.terrain.GridTerrainPosition;
import org.junit.jupiter.api.Test;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

class GridRoverTest {

    @Test
    void constructWithNullPosition() {
        assertThrows(
                NullPointerException.class,
                () -> new GridRoverImpl(null, new GridTerrain(0, 0))
        );
    }

    @Test
    void constructWithNullTerrain() {
        assertThrows(
                NullPointerException.class,
                () -> new GridRoverImpl(new GridTerrainPosition(), null)
        );
    }

    @Test
    void constructWithBlockedLocation() {
        GridTerrain terrain = new GridTerrain(5, 5);
        terrain.blockPosition(new GridTerrainPosition(1, 2));
        assertThrows(
                IllegalArgumentException.class,
                () -> new GridRoverImpl(new GridTerrainPosition(1, 2), terrain)
        );
    }

    @Test
    void constructWithOutOfGridLocation() {
        GridTerrain terrain = new GridTerrain(5, 5);
        terrain.blockPosition(new GridTerrainPosition(1, 2));
        assertThrows(
                IllegalArgumentException.class,
                () -> new GridRoverImpl(new GridTerrainPosition(10, 20), terrain)
        );
    }

    @Test
    void constructWithInvalidHeading() {
        GridTerrain terrain = new GridTerrain(5, 5);
        terrain.blockPosition(new GridTerrainPosition(1, 2));
        assertThrows(
                NullPointerException.class,
                () -> new GridRoverImpl(new GridTerrainPosition(10, 20), null, terrain)
        );
    }

    @Test
    void constructValid() {
        GridRoverImpl rover = new GridRoverImpl(new GridTerrainPosition(), new GridTerrain(0, 0));
        assertThat(rover.getCurrentPosition()).isEqualTo(new GridTerrainPosition());
        assertThat(rover.getHeading()).isEqualTo(GridHeading.NORTH);
    }

    @Test
    void constructValid_withHeading() {
        GridRoverImpl rover = new GridRoverImpl(new GridTerrainPosition(), GridHeading.WEST, new GridTerrain(0, 0));
        assertThat(rover.getCurrentPosition()).isEqualTo(new GridTerrainPosition());
        assertThat(rover.getHeading()).isEqualTo(GridHeading.WEST);
    }

    @Test
    void turnLeftFromNorth() {
        GridRoverImpl rover = new GridRoverImpl(new GridTerrainPosition(), GridHeading.NORTH, new GridTerrain(0, 0));
        rover.turnLeft();
        assertThat(rover.getHeading()).isEqualTo(GridHeading.WEST);
    }

    @Test
    void turnLeftFromWest() {
        GridRoverImpl rover = new GridRoverImpl(new GridTerrainPosition(), GridHeading.WEST, new GridTerrain(0, 0));
        rover.turnLeft();
        assertThat(rover.getHeading()).isEqualTo(GridHeading.SOUTH);
    }

    @Test
    void turnLeftFromSouth() {
        GridRoverImpl rover = new GridRoverImpl(new GridTerrainPosition(), GridHeading.SOUTH, new GridTerrain(0, 0));
        rover.turnLeft();
        assertThat(rover.getHeading()).isEqualTo(GridHeading.EAST);
    }

    @Test
    void turnLeftFromEast() {
        GridRoverImpl rover = new GridRoverImpl(new GridTerrainPosition(), GridHeading.EAST, new GridTerrain(0, 0));
        rover.turnLeft();
        assertThat(rover.getHeading()).isEqualTo(GridHeading.NORTH);
    }

    @Test
    void turnRightFromNorth() {
        GridRoverImpl rover = new GridRoverImpl(new GridTerrainPosition(), GridHeading.NORTH, new GridTerrain(0, 0));
        rover.turnRight();
        assertThat(rover.getHeading()).isEqualTo(GridHeading.EAST);
    }

    @Test
    void turnRightFromEast() {
        GridRoverImpl rover = new GridRoverImpl(new GridTerrainPosition(), GridHeading.EAST, new GridTerrain(0, 0));
        rover.turnRight();
        assertThat(rover.getHeading()).isEqualTo(GridHeading.SOUTH);
    }

    @Test
    void turnRightFromSouth() {
        GridRoverImpl rover = new GridRoverImpl(new GridTerrainPosition(), GridHeading.SOUTH, new GridTerrain(0, 0));
        rover.turnRight();
        assertThat(rover.getHeading()).isEqualTo(GridHeading.WEST);
    }

    @Test
    void turnRightFromWest() {
        GridRoverImpl rover = new GridRoverImpl(new GridTerrainPosition(), GridHeading.WEST, new GridTerrain(0, 0));
        rover.turnRight();
        assertThat(rover.getHeading()).isEqualTo(GridHeading.NORTH);
    }

    @Test
    void moveNorth() {
        GridTerrain terrain = new GridTerrain(10, 10);
        GridTerrainPosition initialPosition = new GridTerrainPosition(5, 5);
        GridRoverImpl rover = new GridRoverImpl(initialPosition, GridHeading.NORTH, terrain);
        rover.move();
        assertThat(rover.getCurrentPosition()).isEqualTo(new GridTerrainPosition(5, 6));
        assertThat(rover.getHeading()).isEqualTo(GridHeading.NORTH);

        assertThat(terrain.isBlocked(rover.getCurrentPosition())).isTrue();
        assertThat(terrain.isBlocked(initialPosition)).isFalse();
    }

    @Test
    void moveEast() {
        GridTerrain terrain = new GridTerrain(10, 10);
        GridTerrainPosition initialPosition = new GridTerrainPosition(5, 5);
        GridRoverImpl rover = new GridRoverImpl(initialPosition, GridHeading.EAST, terrain);
        rover.move();
        assertThat(rover.getCurrentPosition()).isEqualTo(new GridTerrainPosition(6, 5));
        assertThat(rover.getHeading()).isEqualTo(GridHeading.EAST);

        assertThat(terrain.isBlocked(rover.getCurrentPosition())).isTrue();
        assertThat(terrain.isBlocked(initialPosition)).isFalse();
    }

    @Test
    void moveSouth() {
        GridTerrain terrain = new GridTerrain(10, 10);
        GridTerrainPosition initialPosition = new GridTerrainPosition(5, 5);
        GridRoverImpl rover = new GridRoverImpl(initialPosition, GridHeading.SOUTH, terrain);
        rover.move();
        assertThat(rover.getCurrentPosition()).isEqualTo(new GridTerrainPosition(5, 4));
        assertThat(rover.getHeading()).isEqualTo(GridHeading.SOUTH);

        assertThat(terrain.isBlocked(rover.getCurrentPosition())).isTrue();
        assertThat(terrain.isBlocked(initialPosition)).isFalse();
    }

    @Test
    void moveWest() {
        GridTerrain terrain = new GridTerrain(10, 10);
        GridTerrainPosition initialPosition = new GridTerrainPosition(5, 5);
        GridRoverImpl rover = new GridRoverImpl(initialPosition, GridHeading.WEST, terrain);
        rover.move();
        assertThat(rover.getCurrentPosition()).isEqualTo(new GridTerrainPosition(4, 5));
        assertThat(rover.getHeading()).isEqualTo(GridHeading.WEST);

        assertThat(terrain.isBlocked(rover.getCurrentPosition())).isTrue();
        assertThat(terrain.isBlocked(initialPosition)).isFalse();
    }

    @Test
    void moveOutOfGrid() {
        GridRoverImpl rover = new GridRoverImpl(new GridTerrainPosition(0, 5), GridHeading.WEST, new GridTerrain(10, 10));
        assertThrows(IllegalStateException.class, () -> rover.move());
    }

    @Test
    void moveToBlocked() {
        GridTerrain terrain = new GridTerrain(10, 10);
        terrain.blockPosition(new GridTerrainPosition(5, 6));
        GridRoverImpl rover = new GridRoverImpl(new GridTerrainPosition(5, 5), GridHeading.NORTH, terrain);
        assertThrows(IllegalStateException.class, () -> rover.move());
    }

    @Test
    void executeCommand() {
        //TODO: write test
    }
}