package com.space.grid.rover;

import com.space.grid.terrain.GridHeading;
import com.space.grid.terrain.GridTerrain;
import org.junit.jupiter.api.Test;

import static com.google.common.truth.Truth.assertThat;

class GridRoverFactoryImplTest {

    @Test
    void getRover() {
        GridRoverFactoryImpl factory = new GridRoverFactoryImpl();
        GridRoverCriteria criteria = new GridRoverCriteria();
        criteria.setHeading(GridHeading.NORTH);
        criteria.setX(1);
        criteria.setY(2);
        criteria.setTerrain(new GridTerrain(6, 6));
        GridRover rover = factory.getRover(criteria);
        assertThat(rover.getCurrentPosition().getX()).isEqualTo(1);
        assertThat(rover.getCurrentPosition().getY()).isEqualTo(2);
    }
}