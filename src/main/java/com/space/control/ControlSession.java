package com.space.control;

import com.space.rover.Rover;
import com.space.terrain.Terrain;

/**
 * A state session that spans an application lifecycle
 */
public class ControlSession {
    private Terrain terrain;
    private Rover rover;

    /**
     * Initialize the session with a new terrain
     *
     * @param terrain
     */
    public void initialize(Terrain terrain) {
        this.terrain = terrain;
        rover = null;
    }

    /**
     * Get the current terrain in the session
     *
     * @return
     */
    public Terrain getTerrain() {
        return terrain;
    }

    /**
     * Add a rover to the session
     *
     * @param rover
     */
    public void addRover(Rover rover) {
        this.rover = rover;
    }

    /**
     * Get the current rover in the session
     *
     * @return
     */
    public Rover getRover() {
        return rover;
    }
}
