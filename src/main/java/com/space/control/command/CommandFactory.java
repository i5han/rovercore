package com.space.control.command;

import javax.inject.Inject;
import java.util.Set;

/**
 * Factory that provides commands for the given user input.
 */
public class CommandFactory {
    private Set<Command> commands;

    private InvalidCommand invalidCommand;

    /**
     * Provide the command to process the user input. {@link InvalidCommand} will be returned when a match is not found
     *
     * @param commandString the user input
     * @return the relevant command to process the input
     */
    public Command getCommand(String commandString) {
        for (Command command : commands) {
            if (command.getPattern().matcher(commandString).matches()) {
                return command;
            }
        }
        return invalidCommand;
    }

    @Inject
    public void setCommands(Set<Command> commands) {
        this.commands = commands;
    }

    @Inject
    public void setInvalidCommand(InvalidCommand invalidCommand) {
        this.invalidCommand = invalidCommand;
    }
}
