package com.space.control.command;

import java.util.regex.Pattern;

class QuitCommand implements Command {
    @Override
    public Pattern getPattern() {
        return Pattern.compile("^(exit)$|^[qQ]$");
    }

    @Override
    public void execute(String command) {
        System.exit(0);
    }
}
