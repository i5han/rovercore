package com.space.control.command;

import com.google.common.base.Functions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.space.control.ControlSession;
import com.space.control.Display;
import com.space.grid.rover.*;
import com.space.grid.terrain.GridTerrainPosition;
import com.space.rover.Rover;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public class NavigateGridRoverCommand implements Command {

    private Display display;

    private ControlSession session;

    private static final Map<String, GridRoverCommand> COMMAND_MAPPING = ImmutableMap.of(
            "L", new GridRoverTurnLeft(),
            "R", new GridRoverTurnRight(),
            "M", new GridRoverMove()
    );

    @Override
    public Pattern getPattern() {
        return Pattern.compile("^[LRMlrm]+$");
    }

    @Override
    public void execute(String command) {
        Rover rover = session.getRover();
        if (rover == null) {
            display.showMessage("Rover needs to be added. use command [x y heading] without brackets to add a rover at position x y facing heading");
            return;
        }
        if (!(rover instanceof GridRover)) {
            display.showMessage("{0} rover cannot process the given commands", rover.getClass());
            return;
        }

        GridRover gridRover = (GridRover) rover;
        List<GridRoverCommand> commands = getRoverCommands(command);
        rover.perform(commands);

        GridTerrainPosition currentPosition = gridRover.getCurrentPosition();
        display.showMessage("{0} {1} {2}", currentPosition.getX(), currentPosition.getY(), gridRover.getHeading().getShortCode());
    }

    private List<GridRoverCommand> getRoverCommands(String command) {
        return Arrays.stream(command.split(""))
                .map(String::toUpperCase)
                .map(Functions.forMap(COMMAND_MAPPING))
                .collect(ImmutableList.toImmutableList());
    }

    @Inject
    public void setDisplay(Display display) {
        this.display = display;
    }

    @Inject
    public void setSession(ControlSession session) {
        this.session = session;
    }
}
