package com.space.control.command;

import com.space.control.ControlSession;
import com.space.control.Display;
import com.space.grid.rover.GridRover;
import com.space.grid.rover.GridRoverCriteria;
import com.space.grid.rover.GridRoverFactory;
import com.space.grid.rover.GridRoverImpl;
import com.space.grid.terrain.GridHeading;
import com.space.grid.terrain.GridTerrain;

import javax.inject.Inject;
import java.util.regex.Pattern;

/**
 * A command that creates a {@link GridRoverImpl}
 */
class AddGridRoverCommand implements Command {
    private ControlSession session;

    private Display display;

    private GridRoverFactory roverFactory;

    @Override
    public Pattern getPattern() {
        return Pattern.compile("^\\d+\\s+\\d+\\s+[NEWSnews]$");
    }

    @Override
    public void execute(String command) {
        if (session.getTerrain() == null) {
            display.showMessage("Terrain needs to be initialized. use command [x y] without brackets to initialize a grid terrain of size x,y");
            return;
        }
        String[] roverCriteria = command.split("\\s+");

        GridRover rover = roverFactory.getRover(new GridRoverCriteria(
                Long.valueOf(roverCriteria[0]),
                Long.valueOf(roverCriteria[1]),
                GridHeading.codeToHeading(roverCriteria[2].toUpperCase().charAt(0)),
                ((GridTerrain) session.getTerrain())
        ));

        session.addRover(rover);
        display.showMessage("Rover deployed. Ready to receive command sequence");
    }

    @Inject
    public void setSession(ControlSession session) {
        this.session = session;
    }

    @Inject
    public void setDisplay(Display display) {
        this.display = display;
    }

    @Inject
    public void setRoverFactory(GridRoverFactory roverFactory) {
        this.roverFactory = roverFactory;
    }
}
