package com.space.control.command;

import com.space.control.ControlSession;
import com.space.control.Display;
import com.space.grid.terrain.GridTerrain;

import javax.inject.Inject;
import java.util.regex.Pattern;

/**
 * Command that constructs a grid terrain.
 */
class GridConstructCommand implements Command {

    private ControlSession session;

    private Display display;

    @Override
    public Pattern getPattern() {
        return Pattern.compile("^\\d+\\s+\\d+$");
    }

    @Override
    public void execute(String command) {
        String[] coordinates = command.split("\\s+");
        session.initialize(new GridTerrain(Long.valueOf(coordinates[0]), Long.valueOf(coordinates[1])));
        display.showMessage("Terrain initialized. Ready to deploy rover.");
    }

    @Inject
    public void setSession(ControlSession session) {
        this.session = session;
    }

    @Inject
    public void setDisplay(Display display) {
        this.display = display;
    }
}
