package com.space.control.command;

import com.space.control.Display;

import javax.inject.Inject;
import java.util.regex.Pattern;

class InvalidCommand implements Command {

    @Inject
    private Display display;

    @Override
    public Pattern getPattern() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void execute(String command) {
        display.showMessage("{0} command is not known. please check.", command);
    }
}
