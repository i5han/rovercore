package com.space.control.command;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import com.google.inject.multibindings.Multibinder;
import com.space.control.ControlSession;

public class CommandConfiguration extends AbstractModule {

    @Override
    protected void configure() {
        bind(ControlSession.class).in(Singleton.class);
        Multibinder<Command> multibinder = Multibinder.newSetBinder(binder(), Command.class);
        multibinder.addBinding().to(GridConstructCommand.class);
        multibinder.addBinding().to(QuitCommand.class);
        multibinder.addBinding().to(AddGridRoverCommand.class);
        multibinder.addBinding().to(NavigateGridRoverCommand.class);
    }

}
