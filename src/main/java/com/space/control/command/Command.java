package com.space.control.command;

import java.util.regex.Pattern;

/**
 * A command that a user issues via the command line
 */
public interface Command {
    /**
     * The pattern that should match a user input from the command line
     *
     * @return
     */
    Pattern getPattern();

    /**
     * execute the user input command
     *
     * @param command
     */
    void execute(String command);
}
