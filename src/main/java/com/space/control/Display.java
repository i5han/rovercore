package com.space.control;

public interface Display {
    void showMessage(String message);

    void showMessage(String format, Object... values);
}
