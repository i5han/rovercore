package com.space;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;
import com.space.control.Display;
import com.space.control.command.Command;
import com.space.control.command.CommandFactory;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ServiceLoader;

public class Main {
    private static Display display;
    private static CommandFactory commandFactory;

    public static void main(String[] args) {
        Injector injector = Guice.createInjector(ServiceLoader.load(Module.class));
        commandFactory = injector.getInstance(CommandFactory.class);
        display = injector.getInstance(Display.class);

        display.showMessage("Initialize terrain. eg command '5 5'");
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in))) {
            String commandString;
            while ((commandString = bufferedReader.readLine()) != null) {
                runCommand(commandString);
            }
        } catch (Exception e) {
            display.showMessage(e.getMessage());
        }
    }

    private static void runCommand(String commandString) {
        try {
            Command command = commandFactory.getCommand(commandString);
            command.execute(commandString);
        } catch (Exception e) {
            display.showMessage(e.getMessage());
        }
    }
}
