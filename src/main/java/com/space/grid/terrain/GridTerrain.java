package com.space.grid.terrain;

import com.google.common.collect.Range;
import com.space.terrain.Terrain;

import java.util.HashSet;
import java.util.Set;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Grid Terrain is a bounded rectangular grid with discrete x,y coordinates.
 */
public class GridTerrain implements Terrain<GridTerrainPosition> {

    private Range<Long> xRange;
    private Range<Long> yRange;

    /* HashSet used for O(1) contains operation when checking for blocks.*/
    private Set<GridTerrainPosition> blockedPositions = new HashSet<>();

    /**
     * Grid Terrain will have lower bounds of (0,0). Upper bounds will be inclusive
     *
     * @param xBounds positive integer. max is Long.MAX_VALUE
     * @param yBounds positive integer. max is Long.MAX_VALUE
     * @throws IllegalArgumentException if coordinates are negative
     */
    public GridTerrain(long xBounds, long yBounds) {
        checkArgument(xBounds >= 0, "x grid coordinates must not be negative");
        checkArgument(yBounds >= 0, "y grid coordinates must not be negative");

        xRange = Range.closed(0L, xBounds);
        yRange = Range.closed(0L, yBounds);
    }

    @Override
    public boolean withinBounds(GridTerrainPosition position) {
        checkNotNull(position, "Position must be not null");
        return xRange.contains(position.getX()) && yRange.contains(position.getY());
    }

    @Override
    public void blockPosition(GridTerrainPosition position) {
        checkNotNull(position, "Position must be not null");
        checkArgument(withinBounds(position), "Position must be within the grid");

        blockedPositions.add(position);
    }

    @Override
    public void unblockPosition(GridTerrainPosition position) {
        checkNotNull(position, "Position must be not null");
        checkArgument(withinBounds(position), "Position must be within the grid");

        blockedPositions.remove(position);
    }

    @Override
    public boolean isBlocked(GridTerrainPosition position) {
        checkNotNull(position, "Position must be not null");
        checkArgument(withinBounds(position), "Position must be within the grid");

        return blockedPositions.contains(position);
    }

    @Override
    public boolean isPositionValid(GridTerrainPosition position) {
        checkNotNull(position, "Position must be not null");

        return withinBounds(position) && !isBlocked(position);
    }
}
