package com.space.grid.terrain;

import com.space.terrain.Position;

import java.util.Objects;

public class GridTerrainPosition implements Position {
    private long x;
    private long y;

    public GridTerrainPosition() {
    }

    public GridTerrainPosition(long x, long y) {
        this.x = x;
        this.y = y;
    }

    public long getX() {
        return x;
    }

    public long getY() {
        return y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GridTerrainPosition that = (GridTerrainPosition) o;
        return x == that.x &&
                y == that.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    @Override
    public String toString() {
        return "{x=" + x + ", y=" + y + '}';
    }
}
