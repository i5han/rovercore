package com.space.grid.terrain;

import com.google.common.collect.ImmutableMap;

import java.util.Map;

public enum GridHeading {
    NORTH(0, 'N'), EAST(90, 'E'), SOUTH(180, 'S'), WEST(270, 'W');

    private int angle;

    private char shortCode;

    private static final Map<Integer, GridHeading> MAPPING = ImmutableMap.of(
            NORTH.angle, NORTH,
            EAST.angle, EAST,
            SOUTH.angle, SOUTH,
            WEST.angle, WEST
    );

    private static final Map<Character, GridHeading> CODE_MAPPING = ImmutableMap.of(
            NORTH.shortCode, NORTH,
            EAST.shortCode, EAST,
            SOUTH.shortCode, SOUTH,
            WEST.shortCode, WEST
    );

    GridHeading(int angle, char shortCode) {
        this.angle = angle;
        this.shortCode = shortCode;
    }

    public static GridHeading angleToHeading(int angle) {
        return MAPPING.get(angle);
    }

    public static GridHeading codeToHeading(char shortCode) {
        return CODE_MAPPING.get(shortCode);
    }

    public int getAngle() {
        return angle;
    }

    public char getShortCode() {
        return shortCode;
    }
}
