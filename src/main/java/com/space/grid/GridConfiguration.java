package com.space.grid;

import com.google.inject.AbstractModule;
import com.space.grid.rover.GridRoverFactory;
import com.space.grid.rover.GridRoverFactoryImpl;

public class GridConfiguration extends AbstractModule {
    @Override
    protected void configure() {
        bind(GridRoverFactory.class).to(GridRoverFactoryImpl.class);
    }
}
