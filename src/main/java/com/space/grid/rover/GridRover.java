package com.space.grid.rover;

import com.space.grid.terrain.GridHeading;
import com.space.grid.terrain.GridTerrainPosition;
import com.space.rover.Rover;

/**
 * A Rover that can only navigate in a discrete grid.
 * This rover can only turn at 90deg angles and move forward a unit amount at a time.
 * Command execution will halt at the first failure
 */
public interface GridRover extends Rover<GridRoverCommand> {
    /**
     * Turn 90deg counter-clockwise.
     */
    void turnLeft();

    /**
     * Turn 90deg clockwise
     */
    void turnRight();

    /**
     * Move a unit in the facing direction
     *
     * @throws IllegalStateException if the new position is not clear
     */
    void move();

    @Override
    GridTerrainPosition getCurrentPosition();

    /**
     * @return the current heading of the rover
     */
    GridHeading getHeading();
}
