package com.space.grid.rover;

import com.space.grid.terrain.GridTerrainPosition;

public class GridRoverFactoryImpl implements GridRoverFactory {
    @Override
    public GridRover getRover(GridRoverCriteria criteria) {
        return new GridRoverImpl(
                new GridTerrainPosition(criteria.getX(), criteria.getY()),
                criteria.getHeading(),
                criteria.getTerrain()
        );
    }
}
