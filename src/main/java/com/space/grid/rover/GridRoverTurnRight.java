package com.space.grid.rover;

public class GridRoverTurnRight implements GridRoverCommand {
    @Override
    public boolean execute(GridRover rover) {
        rover.turnRight();
        return true;
    }
}
