package com.space.grid.rover;

import com.space.rover.RoverFactory;

public interface GridRoverFactory extends RoverFactory<GridRover, GridRoverCriteria> {
}
