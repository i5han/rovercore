package com.space.grid.rover;

public class GridRoverTurnLeft implements GridRoverCommand {
    @Override
    public boolean execute(GridRover rover) {
        rover.turnLeft();
        return true;
    }
}
