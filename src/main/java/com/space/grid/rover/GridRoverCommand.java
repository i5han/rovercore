package com.space.grid.rover;

import com.space.rover.RoverCommand;

public interface GridRoverCommand extends RoverCommand {
    boolean execute(GridRover rover);
}
