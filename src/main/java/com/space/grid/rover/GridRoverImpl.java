package com.space.grid.rover;

import com.google.common.base.MoreObjects;
import com.google.common.collect.ImmutableList;
import com.space.grid.terrain.GridHeading;
import com.space.grid.terrain.GridTerrain;
import com.space.grid.terrain.GridTerrainPosition;

import java.util.List;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.lang.Math.*;

/**
 * Default implementation of {@link GridRover}
 */
public class GridRoverImpl implements GridRover {

    private GridTerrainPosition currentPosition;

    private GridTerrain terrain;

    private int heading;

    private static final int TURN_ANGLE = 90;
    private static final int FULL_CIRCLE_ANGLE = 360;

    public static final int HEADING_WEST = 270;

    /**
     * @param currentPosition the initial position of the rover in the grid
     * @param terrain         the terrain on which this rover is located
     * @throws NullPointerException     if currentPosition or terrain is null
     * @throws IllegalArgumentException if the position is not valid within the terrain
     */
    public GridRoverImpl(GridTerrainPosition currentPosition, GridTerrain terrain) {
        this(currentPosition, GridHeading.NORTH, terrain);
    }

    /**
     * @param currentPosition the initial position of the rover in the grid
     * @param terrain         the terrain on which this rover is located
     * @param heading         the initial heading of the rover in degrees
     * @throws NullPointerException     if currentPosition or terrain is null
     * @throws IllegalArgumentException if the position is not valid within the terrain
     * @throws IllegalArgumentException if the heading is not valid.
     */
    public GridRoverImpl(GridTerrainPosition currentPosition, GridHeading heading, GridTerrain terrain) {
        checkNotNull(currentPosition, "Current position cannot be null");
        checkNotNull(terrain, "Terrain cannot be null");
        checkNotNull(heading, "Heading cannot be null");
        checkArgument(terrain.isPositionValid(currentPosition), "The rover cannot be placed at {}. Location not valid", currentPosition.toString());

        this.currentPosition = currentPosition;
        this.terrain = terrain;
        this.heading = heading.getAngle();

        terrain.blockPosition(currentPosition);
    }

    @Override
    public void perform(GridRoverCommand command) {
        if (command != null) {
            command.execute(this);
        }
    }

    @Override
    public void perform(List<GridRoverCommand> commands) {
        commands = MoreObjects.firstNonNull(commands, ImmutableList.of());
        for (GridRoverCommand command : commands) {
            command.execute(this);
        }
    }

    @Override
    public void turnLeft() {
        heading = (heading == 0) ? HEADING_WEST : heading - TURN_ANGLE;
    }

    /**
     * Turn 90deg clockwise
     */
    @Override
    public void turnRight() {
        heading = (heading + TURN_ANGLE) % FULL_CIRCLE_ANGLE;
    }


    @Override
    public void move() {
        GridTerrainPosition newPosition = new GridTerrainPosition(
                currentPosition.getX() + (long) sin(toRadians(heading)),
                currentPosition.getY() + (long) cos(toRadians(heading))
        );

        if (terrain.isPositionValid(newPosition)) {
            moveToPosition(newPosition);
        } else {
            throw new IllegalStateException("Unable to move to location " + newPosition + " from location " + currentPosition);
        }
    }

    private void moveToPosition(GridTerrainPosition newPosition) {
        terrain.blockPosition(newPosition);
        terrain.unblockPosition(currentPosition);
        currentPosition = newPosition;
    }

    /**
     * Get the current location of the rover
     *
     * @return the currentPosition of the rover
     */
    public GridTerrainPosition getCurrentPosition() {
        return currentPosition;
    }

    @Override
    public GridHeading getHeading() {
        return GridHeading.angleToHeading(heading);
    }

    @Override
    public String toString() {
        return "{currentPosition=" + currentPosition + ", heading=" + heading + '}';
    }
}
