package com.space.grid.rover;

import com.space.grid.terrain.GridHeading;
import com.space.grid.terrain.GridTerrain;
import com.space.rover.RoverCriteria;

public class GridRoverCriteria implements RoverCriteria {
    private long x;
    private long y;
    private GridHeading heading;
    private GridTerrain terrain;

    public GridRoverCriteria() {
    }

    public GridRoverCriteria(long x, long y, GridHeading heading, GridTerrain terrain) {
        this.x = x;
        this.y = y;
        this.heading = heading;
        this.terrain = terrain;
    }

    public long getX() {
        return x;
    }

    public void setX(long x) {
        this.x = x;
    }

    public long getY() {
        return y;
    }

    public void setY(long y) {
        this.y = y;
    }

    public GridHeading getHeading() {
        return heading;
    }

    public void setHeading(GridHeading heading) {
        this.heading = heading;
    }

    public GridTerrain getTerrain() {
        return terrain;
    }

    public void setTerrain(GridTerrain terrain) {
        this.terrain = terrain;
    }
}
