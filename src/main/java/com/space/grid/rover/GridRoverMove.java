package com.space.grid.rover;

public class GridRoverMove implements GridRoverCommand {
    @Override
    public boolean execute(GridRover rover) {
        rover.move();
        return true;
    }
}
