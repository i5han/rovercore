package com.space.rover;

public interface RoverFactory<T extends Rover, C extends RoverCriteria> {
    T getRover(C criteria);
}
