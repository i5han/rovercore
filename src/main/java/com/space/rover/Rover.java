package com.space.rover;

import com.space.terrain.Position;

import java.util.List;

/**
 * A rover that moves around a {@link com.space.terrain.Terrain}
 * roves can execute a single {@link RoverCommand} or a list of commands in sequence
 *
 * @param <T>
 */
public interface Rover<T extends RoverCommand> {

    /**
     * Perform the provided command on the rover
     *
     * @param command
     */
    void perform(T command);

    /**
     * Perform the list of commands on the rover.
     *
     * @param commands a list of commands
     */
    void perform(List<T> commands);

    /**
     * Get the current location of the rover
     *
     * @return
     */
    Position getCurrentPosition();
}
