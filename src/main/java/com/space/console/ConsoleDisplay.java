package com.space.console;

import com.space.control.Display;

import java.text.MessageFormat;

public class ConsoleDisplay implements Display {
    @Override
    public void showMessage(String message) {
        System.out.println(message);
    }

    @Override
    public void showMessage(String format, Object... values) {
        System.out.println(MessageFormat.format(format, values));
    }
}
