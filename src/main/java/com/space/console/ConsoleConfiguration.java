package com.space.console;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import com.space.control.Display;

public class ConsoleConfiguration extends AbstractModule {
    @Override
    protected void configure() {
        bind(Display.class).to(ConsoleDisplay.class).in(Singleton.class);
    }
}
