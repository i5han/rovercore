package com.space.terrain;

/**
 * Terrain represents a bounded area. The area may contain positions that are not traversable, which is indicated by blocking said positions.
 */
public interface Terrain<T extends Position> {
    /**
     * Indicates whether the given position is within the bounded area.
     *
     * @param position
     * @return
     */
    boolean withinBounds(T position);

    /**
     * Mark the given position as blocked.
     *
     * @param position
     */
    void blockPosition(T position);

    /**
     * Mark the given position as unblocked.
     *
     * @param position
     */
    void unblockPosition(T position);

    /**
     * Indicates whether the given position is blocked
     *
     * @param position
     * @return
     */
    boolean isBlocked(T position);

    /**
     * Indicates that the given position is clear of obstacles and within bounds of the area.
     *
     * @param position
     * @return
     */
    boolean isPositionValid(T position);
}
