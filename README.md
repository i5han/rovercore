## Overview ##
Rover core is a program to manipulate rovers on a terrain. At the moment it supports navigating rovers on a Rectangular grid.

## Prerequisites ##
* jdk8 installation available on the path. Tested on jdk 1.8.0_171.
* maven3 installation available on the path. Tested on maven 3.5.4

## Building and Running ##
1. Run command `mvn clean package` to build the project.
2. Extract the `target\rover-core.zip` to a convenient location and run using the `launch.bat | launch.sh` script.
3. Tests can be run using `mvn test`.
4. Built and tested on Windows 10 build 1803

## Commands ##
* Initialize grid: `X Y` where x and y are non-negative integers. Max value of `Long.MAX_VALUE`
* Add rover: `X Y heading` where x and y are non-negative integers. Max value of `Long.MAX_VALUE`. heading is one of `[N|E|W|S]` case insensitive
* Navigate rover: A string containing one or more of `[L|R|M]`. case insensitive 

## Assumptions ##
* Deploying a new rover will not clear state of the previous rover. Therefore the last location of the previous rovers are blocked.
* There are no SLA performance targets.
* Access control/security will be handled externally
* The maximum dimensions of a rover will be smaller than the grid block size. This is related to blocking the current location of the rover on the terrain.
* Grid x,y scales are the same.
* Grid scale and resolution for the plateau should be decided by the user.
* The order of adding and controlling rovers on a surface is the same regardless of terrain type and rover type.
* Language will be english only.

## Limitations ##
* Does not persist state across application restarts.
* Does not ensure thread safety.
* Number of rovers/number of commands is bounded within the memory constraints of the host system. (supports only vertical scalability)
* Upper bound on grid size is Long.MAX_VALUE. Users should take precautions against overflows.